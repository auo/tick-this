# tick-this ✔
⚠ **Deprecated** ⚠
Small CLI to get date from timestamp, either millis or seconds.

# install 🔨
Clone the repo and install with `npm i tick-this`


# how to use 💻 
* miliseconds `tick-this 1539094316611`
* seconds `tick-this 1539094316 -s`

