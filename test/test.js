const test = require('tape');
const tick = require('../src/tick-this');

test('miliseconds', (t) => {
    t.plan(1);
    const timestamp = 1539102762257;
    const result = tick({ command: timestamp });
    t.equal(result, '2018-10-09T16:32:42.257Z');
});