#!/usr/bin/env node
const commandLineArgs = require('command-line-args');

const optionDefinition = [
	{ name: 'command', defaultOption: true, type: Number },
	{ name: 'seconds', alias: 's', type: Boolean } ];

const options = commandLineArgs(optionDefinition);

console.log(require('./src/tick-this')(options));
