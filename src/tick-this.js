module.exports = (options) => {
    return new Date(options.command * (options.seconds ? 1000 : 1)).toISOString();
}
